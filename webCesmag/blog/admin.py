from django.contrib import admin
from .models import Category, Article, Document, Location, Organizer


# clase categoryadmin sirve para ordenar la lista
class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', )
    search_fields = ('name', 'description')
    # list_filter=('visible',)
    list_display = ('name', 'created_at')
    ordering = ('-created_at', )


# clase categoryadmin sirve para ordenar la lista
class OrganizerAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', )
    search_fields = ('name', 'description')
    # list_filter=('visible',)
    list_display = ('name', 'created_at')
    ordering = ('-created_at', )


# clase categorylocation sirve para ordenar la lista
class LocationAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', )
    search_fields = ('name', 'description')
    # list_filter=('visible',)
    list_display = ('name', 'created_at')
    ordering = ('-created_at', )


# clase Document sirve para ordenar la lista
class DocumentAdmin(admin.ModelAdmin):
    search_fields = ('archivo', )
    # list_filter=('visible',)


# clase ArticleAdmin sirve para ordenar los articulos
class ArticleAdmin(admin.ModelAdmin):
    readonly_fields = ('user', 'created_at', 'update_at')
    search_fields = ('title', 'content', 'user__username', 'Categories__name')
    list_filter = ('public', 'Categories')
    list_display = ('title', 'public', 'created_at', 'user')
    ordering = ('-created_at', )

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user_id = request.user.id
        obj.save()


# Register your models here.
admin.site.register(Category, CategoryAdmin)
admin.site.register(Organizer, OrganizerAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Document, DocumentAdmin)
